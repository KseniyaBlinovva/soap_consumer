package com.epam.service;

import com.epam.domain.Product;
import com.epam.wsdl.ProductInParams;
import com.epam.wsdl.ProductOutResultSetRow;
import com.epam.wsdl.ProductRequest;
import com.epam.wsdl.ProductResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {
    private WebServiceTemplate webServiceTemplate;

    @Autowired
    public ProductService(WebServiceTemplate webServiceTemplate) {
        this.webServiceTemplate = webServiceTemplate;
    }

    public List<Product> searchProduct(String query) {
        ProductRequest request = new ProductRequest();
        ProductInParams inParams = new ProductInParams();
        inParams.setName(query);
        request.setInParams(inParams);
        ProductResponse response =
                (ProductResponse) webServiceTemplate
                        .marshalSendAndReceive(request);
        return response.getOutParams().getResultSet().getResultSetRow().stream().map(this::mapRow).collect(Collectors.toList());
    }

    private Product mapRow(ProductOutResultSetRow row) {
        Product product = new Product();
        product.setName(row.getName());
        return product;
    }
}
