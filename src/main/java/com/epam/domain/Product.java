package com.epam.domain;

import lombok.Data;

@Data
public class Product {
    public String name;
    public int price;
    public String url_img;
}
